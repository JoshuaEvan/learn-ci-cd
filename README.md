Learn CI/CD by experimenting with several cases:

- CI/CD Hello world
  + use web IDE to add newfile
  + add .gitlab.ci.yml
  + choose HTML from template
  + wait for pipeline successfull then access the pages
  + try to change the file then push the changes
  + again, wait for pipeline successfull then access the pages

- CI/CD Stages
```yaml
image: ruby:2.5

stages:
  - build
  - test
  - deploy

build-app:
  stage: build
  script:
    - echo "Building Project"
    - mkdir public
    - cp -r index.html public
  artifacts:
    paths:
      - public

test-app:
  stage: test
  script:
    - echo "Running Test"
    - test -f public/index.html
  
pages:
  stage: deploy
  script:
    - echo "Deployed to pages"
  artifacts:
    paths:
      - public
```

- CI/CD Parallel Job
```yaml
image: ruby:2.5

stages:
  - build
  - test
  - deploy

build-app:
  stage: build
  script:
    - echo "Building Project"
    - mkdir public
    - cp -r index.html public
  artifacts:
    paths:
      - public

test1-app:
  stage: test
  script:
    - echo "Running Test"
    - test -f public/index.html

test2-app:
  stage: test
  script:
    - echo "Running Test"
    - test -f public/index.html

test3-app:
  stage: test
  script:
    - echo "Running Test"
    - test -f public/index.html
  
pages:
  stage: deploy
  script:
    - echo "Deployed to pages"
  artifacts:
    paths:
      - public

documentation:
  stage: deploy
  script:
    - echo "Deploy documentation"
```

- More on CI/CD configuration
```yaml
image: ruby:2.5

stages:
  - build
  - test
  - deploy

init:
  stage: .pre
  before_script:
    - echo "Prepare Environment"
  variables:
    SECRET_KEY: "abracadabra"
  script:
    - echo $CI_PAGES_DOMAIN
    - echo $SECRET_KEY
    - echo $TOKEN
  
build-app:
  stage: build
  before_script:
    - echo "Building Project"
  script:
    - mkdir public
    - cp -r index.html public
  artifacts:
    paths:
      - public
  interruptible: true

test-app:
  stage: test
  before_script:
    - echo "Running Test"
  script:
    - test -f public/index.html
  only:
    - master
  
pages:
  stage: deploy
  before_script:
    - echo "Start deploy to pages"
  script:
    - echo "Deploying to pages"
  after_script:
    - echo "Deployed to pages"
  artifacts:
    paths:
      - public
  only:
    - master
  when: manual

documentation:
  stage: deploy
  script:
    - echo "Deploy documentation"
  only:
    - master

ping-app:
  stage: .post
  before_script:
    - echo "check if pages accessible"
  script:
    - curl -k "https://almas.hilman.gitlab.io/learn-cicd/"
  only:
    - master
```

### template for HWTML by gitlab

```yml
# This file is a template, and might need editing before it works on your project.
# Full project: https://gitlab.com/pages/plain-html
pages:
  stage: deploy
  script:
    - mkdir .public
    - cp -r * .public
    - mv .public public
  artifacts:
    paths:
      - public
  only:
    - master

```